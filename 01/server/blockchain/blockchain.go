package blockchain

import (
	"crypto/sha256"
	"encoding/hex"
)


// Block keeps block headers
type Block struct {
	Hash          string
	PrevBlockHash string
	Data          string
}

// Blockchain keeps a sequence of Blocks
type Blockchain struct {
	Blocks []*Block
}

// setHash calculates and sets block hash
func (b *Block) setHash() {
	hash := sha256.Sum256([]byte(b.PrevBlockHash + b.Data))
	b.Hash = hex.EncodeToString(hash[:])
}

// NewBlock creates and returns Block
func NewBlock(data string, prevBlockHash string) *Block {
	block := &Block{
		Data: data,
		PrevBlockHash: prevBlockHash,
	}

	block.setHash()
	return block
}


// AddBlock saves provided data as a block in the blockchain
func (bc *Blockchain) AddBlock(data string) *Block {
	prevBlock := bc.Blocks[len(bc.Blocks) - 1]
	newBlock := NewBlock(data, prevBlock.Hash)
	bc.Blocks = append(bc.Blocks, newBlock)

	return  newBlock
}

// NewBlockchain creates a new Blockchain with genesis Block
func NewBlockchain() *Blockchain {
	return &Blockchain{[]*Block{NewGenesisBlock()}}
}

// NewGenesisBlock creates and returns genesis Block
func NewGenesisBlock() *Block {
	return NewBlock("Genesis Block", "")
}









